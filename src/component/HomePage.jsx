import React from 'react';
import './HomePage.scss';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import SvgIcon from '@material-ui/core/SvgIcon';
import Search from '@material-ui/core/SvgIcon/SvgIcon';
import Slide from '@material-ui/core/Slide';
import { SEARCH_URL } from '../index';

const CocktailIcon = () => (
  <SvgIcon viewBox="0 0 512 512">
    <g>
      <g>
        <path d="M380.758,20.021c-51.832,0-94.86,38.398-102.146,88.241H153.05L51.096,0L29.259,20.564l82.587,87.698H28.004 l178.121,199.612v174.131H122.55V512h199.968v-29.995h-86.397V307.874l87.827-98.423c16.766,11.062,36.468,17.047,56.81,17.047 c56.926,0,103.238-46.313,103.238-103.238S437.683,20.021,380.758,20.021z M307.126,183.25H135.119l-40.149-44.993h252.304 L307.126,183.25z" />
      </g>
    </g>
  </SvgIcon>
);

const HomePage = () => (
  <Slide direction="down" in>
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
      className="AppHome"
    >
      <Grid className="MainAppIcon" item>
        <CocktailIcon />
      </Grid>
      <Grid className="MainAppIcon" item>
        <Typography gutterBottom variant="subtitle1">
          CocktailFinder
        </Typography>
      </Grid>
      <Grid item>
        <Link to={SEARCH_URL}>
          <TextField
            id="filled-search"
            type="search"
            margin="normal"
            variant="filled"
            label="Search your favorite cocktail"
            fullWidth
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Search />
                </InputAdornment>
              ),
            }}
          />
        </Link>
      </Grid>
    </Grid>
  </Slide>
);

export default HomePage;
