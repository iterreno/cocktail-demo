import React from 'react';
import PropTypes from 'prop-types';
import './DrinkCard.scss';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Grow from '@material-ui/core/Grow';

const avatarStyle = {
  width: 80,
  height: 80,
};
const DrinkCard = ({ urlImage, title }) => (
  <Grow in>
    <Paper className="DrinkCard">
      <Grid container spacing={1}>
        <Grid item xs={4} lg={2} xl={2} md={2}>
          <Avatar alt={title} src={urlImage} style={avatarStyle} />
        </Grid>
        <Grid item xs={8} lg={10} xl={10} md={10} container>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="center"
            spacing={4}
          >
            <Grid item>
              <Typography gutterBottom variant="subtitle1">
                {title}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  </Grow>
);


DrinkCard.defaultProps = {
  urlImage: '',
  title: '',
};

DrinkCard.propTypes = {
  urlImage: PropTypes.string,
  title: PropTypes.string,
};

export default DrinkCard;
