import React from 'react';
import { Route, Switch } from 'react-router-dom';
import SearchPage from '../container/SearchPageContainer';
import { HOME_URL, SEARCH_URL } from '../index';
import HomePage from './HomePage';


const App = () => (
  <>
    <Switch>
      <Route
        exact
        path={SEARCH_URL}
        component={() => <SearchPage />}
      />
      <Route
        path={HOME_URL}
        component={() => <HomePage />}
      />
    </Switch>
  </>
);

export default App;
