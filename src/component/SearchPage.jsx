import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './SearchPage.scss';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import LinearProgress from '@material-ui/core/LinearProgress';
import Slide from '@material-ui/core/Slide';
import Paper from '@material-ui/core/Paper';
import Search from '@material-ui/icons/Search';
import ArrowBack from '@material-ui/icons/ArrowBack';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Grow from '@material-ui/core/Grow';
import DrinkCard from './DrinkCard';
import { HOME_URL } from '../index';


class SearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isQuery: false,
    };
  }

  render() {
    const {
      drinks,
      searchDrinks,
      loading,
      clearDrinks,
    } = this.props;
    const { isQuery } = this.state;
    return (
      <>
        <Slide direction="up" in>
          <Grid container direction="column" spacing={0}>
            <Grid item>
              <Paper className="Search">
                <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                >
                  {!isQuery
                   && (
                   <Grid
                     xs={4}
                     lg={4}
                     xl={4}
                     md={4}
                     item
                   >
                     <Link to={HOME_URL}><ArrowBack className="Back" /></Link>
                   </Grid>
                   )}
                  <Grid
                    xs={8}
                    lg={8}
                    xl={8}
                    md={8}
                    item
                  >
                    <TextField
                      id="filled-search"
                      type="search"
                      margin="normal"
                      variant="filled"
                      placeholder="Search"
                      onChange={(event) => {
                        const query = event.target.value;
                        if (query.length > 0) {
                          this.setState({ isQuery: true });
                        } else {
                          this.setState({ isQuery: false });
                        }
                        if (query.length > 2) {
                          searchDrinks(query);
                        }
                      }}
                      fullWidth
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <Search />
                          </InputAdornment>
                        ),
                      }}
                    />
                  </Grid>
                  {isQuery
                   && (
                   <Grid
                     xs={4}
                     lg={4}
                     xl={4}
                     md={4}
                     item
                   >
                     <Grow in>
                       <Button
                         className="Cancel"
                         variant="contained"
                         color="secondary"
                         onMouseDown={clearDrinks}
                         onTouchEnd={clearDrinks}
                       >
                         Cancel
                       </Button>
                     </Grow>
                   </Grid>
                   )}
                </Grid>
              </Paper>
            </Grid>
            <Grid item>
              {loading && <LinearProgress />}
            </Grid>
            <Grid item />
            {drinks && drinks.map((drink) => (
              <Grid item key={`drink-${drink.idDrink}`}>
                <DrinkCard
                  title={drink.strDrink}
                  urlImage={drink.strDrinkThumb}
                />
              </Grid>
            ))}
          </Grid>
        </Slide>
      </>
    );
  }
}

SearchPage.defaultProps = {
  drinks: [],
  loading: false,
};

SearchPage.propTypes = {
  drinks: PropTypes.arrayOf(PropTypes.shape({
    idDrink: PropTypes.string,
    strDrink: PropTypes.string,
    strDrinkThumb: PropTypes.string,
  })),
  searchDrinks: PropTypes.func.isRequired,
  clearDrinks: PropTypes.func.isRequired,
  loading: PropTypes.bool,
};

export default SearchPage;
