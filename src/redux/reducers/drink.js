import {
  CLEAR_DRINKS,
  LOAD_DRINKS,
  LOAD_DRINKS_FAIL,
  LOAD_DRINKS_SUCCESS,
} from '../actions/drink';

const initState = { list: [], loading: false };

export default function (state = initState, action) {
  switch (action.type) {
    case LOAD_DRINKS_SUCCESS:
      return {
        ...state,
        list: action.payload,
        loading: false,
      };
    case LOAD_DRINKS:
      return {
        ...state,
        loading: true,
      };
    case LOAD_DRINKS_FAIL:
      return {
        ...state,
        loading: false,
      };
    case CLEAR_DRINKS:
      return {
        ...state,
        list: [],
      };
    default:
      return state;
  }
}
