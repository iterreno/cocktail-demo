import { combineReducers } from 'redux';
import drink from './drink';

const rootReducer = combineReducers({
  drink,
});

export default rootReducer;
