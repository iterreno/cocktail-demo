import axios from 'axios';

export const LOAD_DRINKS = 'LOAD_DRINKS';
export const CLEAR_DRINKS = 'CLEAR_DRINKS';
export const LOAD_DRINKS_SUCCESS = 'LOAD_DRINKS_SUCCESS';
export const LOAD_DRINKS_FAIL = 'LOAD_DRINKS_FAIL';
const API_DRINKS_ENDPOINT = 'https://www.thecocktaildb.com/api/json/v1/1/search.php';

export function loadDrinks() {
  return {
    type: LOAD_DRINKS,
  };
}

export function clearDrinks() {
  return {
    type: CLEAR_DRINKS,
  };
}

export function loadDrinksSuccess(drinks) {
  return {
    type: LOAD_DRINKS_SUCCESS,
    payload: drinks,
  };
}

export function loadDrinksFail(message) {
  return {
    type: LOAD_DRINKS_FAIL,
    message,
  };
}


export const searchDrinks = (query) => {
  const API_ENDPOINT = `${API_DRINKS_ENDPOINT}?s=${query}`;
  return (dispatch) => {
    dispatch(loadDrinks());
    return axios.get(API_ENDPOINT)
      .then((response) => {
        dispatch(loadDrinksSuccess(response.data.drinks));
      })
      .catch((error) => {
        loadDrinksFail(error);
      });
  };
};
