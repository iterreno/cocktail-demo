import { connect } from 'react-redux';
import SearchPage from '../component/SearchPage';
import { clearDrinks, searchDrinks } from '../redux/actions/drink';

const mapStateToProps = (state) => ({
  drinks: state.drink.list,
  loading: state.drink.loading,
});

const mapDispatchToProps = (dispatch) => ({
  searchDrinks: (query) => dispatch(searchDrinks(query)),
  clearDrinks: () => dispatch(clearDrinks()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
