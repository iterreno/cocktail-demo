# CocktailFinder

Find Cocktails with a quick text search.

## How to run the app local

### `npm start`

Runs the app in the development mode.

## Live demo in Heroku

Only the first time that loads the page could take 20 seconds because is a free server.

https://cocktail-app-demo.herokuapp.com

## Video to see the app demo

https://drive.google.com/file/d/19m8a1xfOQWJLPsloVHQ9lW0nB_9R3lKQ/view?usp=sharing

## Libraries Used

- Material UI: To make the app nice and responsive. More info [Here](https://material-ui.com).
- Redux: For state management.
- Redux-Thunk: For api calls and side effects with Redux.
- PropTypes: To check typed data.
- Airbnb: To reuse eslint rules from Airbnb. 
- Axios: To make request to the API.
- Node sass: Add support with SCSS files.


## How to be more performant if has to manage a big amount of data

To make this possible we can paginate the results with 25 results by page in the client app and in the API we have to create a new paginate results endpoint or utilize and existing one.


